<?php
/**
 *
 * InterNations
 * Task: User management system
 *
 * @author Paweł Świszcz
 * @date 2023-02-06
 *
 */

declare(strict_types=1);

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;
}
