<?php
/**
 *
 * InterNations
 * Task: User management system
 *
 * @author Paweł Świszcz
 * @date 2023-02-06
 *
 */

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Group;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class GroupCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Group::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $actions->remove(Crud::PAGE_INDEX, Action::DELETE);

        $del = Action::new('DELETE', 'Delete')
            ->displayIf(static function ($entity) {
                return 0 === \count($entity->getUsers());
            })->linkToCrudAction(Action::DELETE);

        $actions->add(Crud::PAGE_INDEX, $del);

        return $actions;
    }
}
