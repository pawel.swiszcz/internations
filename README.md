### Dependencies

- composer https://getcomposer.org/
- docker https://www.docker.com/ (docker-compose)
- symfony binary https://symfony.com/download

### Install

- docker-compose up --build
- composer install
- symfony server:start

###
- screenshots in /data

### Admin 
- http://127.0.0.1:4664 -> my base server url
- register new user /register (http://127.0.0.1:46643/register) , mark "Admin" checkbox to register admin role
- route /admin (http://127.0.0.1:46643/admin)
- users table http://127.0.0.1:46643/admin?crudAction=index&crudControllerFqcn=App%5CController%5CAdmin%5CUserCrudController
- groups table http://127.0.0.1:46643/admin?crudAction=index&crudControllerFqcn=App%5CController%5CAdmin%5CGroupCrudController

### Database model

- images in /data/d1.png and /data/d2.png

```
CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB

CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB

CREATE TABLE group_user (group_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A4C98D39FE54D947 (group_id), INDEX IDX_A4C98D39A76ED395 (user_id), PRIMARY KEY(group_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB
```

### Domain model

- image in /data/d3.png

### API Design

```
GET /users 
- Returns a list of all users.

POST /users
- Creates a new user.
- Request body: { "name": "John Doe" }
- Returns the created user.

DELETE /users/{id}
- Deletes a user by id.

POST /groups
- Creates a new group.
- Request body: { "name": "Developers" }
- Returns the created group.

DELETE /groups/{id}
- Deletes a group by id.

POST /users/{user_id}/groups/{group_id}
- Assigns a user to a group.
- Returns the updated user.

DELETE /users/{user_id}/groups/{group_id}
- Removes a user from a group.
- Returns the updated user.
```